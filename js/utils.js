function animateCss(object, { styleProp, from, to, interval }){
    const step = (from - to) / interval;

    const doStep = resolve => {
        from -= step;
        object.style[styleProp] = `${from}px`;

        if (from >= to) {
            return resolve();
        }

        window.requestAnimationFrame(() => doStep(resolve));
    };

    return new Promise(resolve => {
        window.requestAnimationFrame(() => doStep(resolve));
    });
}

function isCollision(obj1, obj2){
    var k1 = obj1.x > obj2.x ? (obj1.x + obj1.radius) - (obj2.x + obj2.radius) : (obj2.x + obj2.radius) - (obj1.x + obj1.radius),
        k2 = obj1.y > obj2.y ? (obj1.y + obj1.radius) - (obj2.y + obj2.radius) : (obj2.y + obj2.radius) - (obj1.y + obj1.radius);

    return obj1.radius + obj2.radius > Math.sqrt(k1*k1 + k2*k2);
}