const UNIT_TYPES_MAP = {
    D: DollarUnit,
    E: EuroUnit,
    P: PoundUnit,
    R: RoubleUnit,
    K: KOUnit,
    B: BlockUnit
};
const KEY_CODES = {
    38: 'UP',
    40: 'DOWN'
};

function Game() {
    this.scene = new Scene(document.querySelector("#scene"));
    this.actor = new Actor(document.querySelector("#actor"));
    this.toasty = new Toasty(document.querySelector("#toasty"));

    this.sceneUnits = [];
    this.addUnitsInterval = 200; // Каждые 200 итераций

    this.map = MAP || [];
    this.nextMapIndex = 0;

    this.currentIteration = 0;

    this.totalScore = 0;
    this.end = false;

    this.objectsForRender = [];

    this.controls = event => {
        if (this.end) {
            return this.unsetControls();
        }

        switch (KEY_CODES[event.keyCode]) {
            case 'UP':
                this.actor.moveUp();
                break;
            case 'DOWN':
                this.actor.moveDown();
                break;
        }
    };

    this.setControls();
}

Game.prototype.doIteration = function () {
    this.objectsForRender = [this.actor, this.scene];

    this.scene.move();

    for (let sceneUnitIndex = 0, sceneUnitsLength = this.sceneUnits.length; sceneUnitIndex < sceneUnitsLength; sceneUnitIndex += 1) {
        const sceneUnit =  this.sceneUnits[sceneUnitIndex];
        let destroyUnit = false;

        if (isCollision(this.actor, sceneUnit)) { // Todo проверять первых
            if(sceneUnit.undestroyable){
                this.end = true;
                return;
            }

            this.addScore(sceneUnit.score);

            if (sceneUnit.toasty) {
                this.toasty.show();
                this.objectsForRender.push(this.toasty);
            }

            destroyUnit = true;
        }

        if (sceneUnit.x <= -100) {
            destroyUnit = true;
        }

        if (destroyUnit) {
            sceneUnit.destroy();
            this.sceneUnits.splice(sceneUnitIndex, 1);
            sceneUnitsLength -= 1;
            continue;
        }

        sceneUnit.move();
        this.objectsForRender.push(sceneUnit);
    }

    if (this.currentIteration % this.addUnitsInterval === 0) {
        this.addUnitsGroup();
        this.nextMapIndex += 1;
    }

    this.currentIteration += 1;

    this.render();
};

Game.prototype.addUnitsGroup = function() {
    const group = this.map[this.nextMapIndex];
    let column = 0, row = 0;

    for (let index = 0, unitsLength = group.pattern.length; index < unitsLength; index += 1) {
        const groupElement = group.pattern[index];

        if (index !== 0 && index % group.columns === 0) {
            row += 1;
            column = 0;
        }

        if (groupElement === EMPTY_UNIT) {
            column += 1;
            continue;
        }

        const UnitConstructor = UNIT_TYPES_MAP[groupElement];
        const unit = new UnitConstructor(column * 50, row * 50);

        this.sceneUnits.push(unit);
        this.objectsForRender.push(unit);

        column += 1;
    }
};

Game.prototype.addScore = function(score) {
    this.totalScore += score;
};

Game.prototype.setControls = function() {
    document.querySelector('body').addEventListener("keyup", this.controls);
};

Game.prototype.unsetControls = function() {
    document.querySelector('body').removeEventListener("keyup", this.controls);
};

Game.prototype.render = function () {
    if (!this.objectsForRender.length) {
        return;
    }

    this.objectsForRender.forEach(object => object.render());

    document.getElementById('score').innerHTML = this.totalScore;
};