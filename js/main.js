window.addEventListener('load', () => {
    document.getElementById('start').addEventListener('click', function(){
        const game = new Game();

        document.getElementById('start-screen').style.display = 'none';

        window.requestAnimationFrame(() => reRender(game));
    });

    document.getElementById('play-again').addEventListener('click', function(){
        location.reload();
    });
}, {
    once: true
});

function reRender(game) {
    if(game.end){
        document.getElementById('final-screen').style.display = 'block';
        document.getElementById('total-score').innerHTML = game.totalScore;
        return;
    }

    game.doIteration.call(game);

    window.requestAnimationFrame(() => reRender(game));
}