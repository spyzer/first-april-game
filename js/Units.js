function Unit(x, y) {
    this.object = null;

    this.x = x + 1000;
    this.y = y;
    this.positioningStep = 5;

    this.width = 50;
    this.heigth = 50;
    this.radius = this.heigth / 2;

    this.score = 0;
    this.className = 'empty';
    this.undestroyable = false;
    this.toasty = false;
}

Unit.prototype.move = function () {
    this.x -= this.positioningStep;
};

Unit.prototype.destroy = function () {
    this.object.remove();
};

Unit.prototype.render = function () {
    if (!this.object) {
        this.object = document.createElement("div");

        this.object.classList.add('unit', this.className);

        document.getElementById('scene').appendChild(this.object);
    }

    this.object.style.top = this.y + "px";
    this.object.style.left = this.x + "px";
};

function DollarUnit(...args) {
    Unit.call(this, ...args);

    this.score = 70;
    this.className = 'unit__coin--dollar';
}

DollarUnit.prototype = Object.create(Unit.prototype);
DollarUnit.prototype.constructor = DollarUnit;

function RoubleUnit(...args) {
    Unit.call(this, ...args);

    this.score = 10;
    this.className = 'unit__coin--rub';
}

RoubleUnit.prototype = Object.create(Unit.prototype);
RoubleUnit.prototype.constructor = RoubleUnit;

function EuroUnit(...args) {
    Unit.call(this, ...args);

    this.score = 75;
    this.className = 'unit__coin--euro';
}

EuroUnit.prototype = Object.create(Unit.prototype);
EuroUnit.prototype.constructor = EuroUnit;

function PoundUnit(...args) {
    Unit.call(this, ...args);

    this.score = 95;
    this.className = 'unit__coin--pound';
}

PoundUnit.prototype = Object.create(Unit.prototype);
PoundUnit.prototype.constructor = PoundUnit;

function BlockUnit(...args) {
    Unit.call(this, ...args);

    this.score = 0;
    this.undestroyable = true;
    this.className = 'unit__block';
}

BlockUnit.prototype = Object.create(Unit.prototype);
BlockUnit.prototype.constructor = BlockUnit;

function KOUnit(...args) {
    Unit.call(this, ...args);

    this.score = -100;
    this.toasty = true;
    this.className = 'unit__ko';
}

KOUnit.prototype = Object.create(Unit.prototype);
KOUnit.prototype.constructor = KOUnit;