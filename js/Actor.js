function Actor($node, options = {}) {
    this.object = $node;

    this.heigth = options.heigth || 100;
    this.radius = this.heigth / 2;

    this.x = 50;
    this.y = 200;
    this.row = 3;

    this.prevX = 0;
    this.prevY = 0;
}

Actor.prototype.moveUp = function () {
    if (this.row <= 0) {
        return;
    }

    this.row -= 1;
    this.y = this.row * this.heigth;
};

Actor.prototype.moveDown = function () {
    if (this.row >= 4) {
        return;
    }

    this.row += 1;
    this.y = this.row * this.heigth;
};

Actor.prototype.render = function () {
    if (this.prevX === this.x && this.prevY === this.y) {
        return;
    }

    this.object.style.left = `${this.x}px`;
    this.object.style.top = `${this.y}px`;
};