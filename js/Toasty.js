function Toasty($node) {
    this.object = $node;

    this.animationStartPosition = -200;
    this.animationEndPosition = 0;
    this.animationInterval = 10;

    this.showing = false;
}

Toasty.prototype.show = function () {
    this.showing = true;

    animateCss(this.object, {
        styleProp: 'right',
        from: this.animationStartPosition,
        to: this.animationEndPosition,
        interval: this.animationInterval
    })
        .then(() => setTimeout(() => {
            this.hide();
        }, 1000));
};

Toasty.prototype.hide = function () {
    this.showing = false;

    this.render();
};

Toasty.prototype.render = function () {
    this.object.style.display = this.showing ? 'block' : 'none';
};