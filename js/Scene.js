function Scene($node, options = {}){
    this.object = $node;
    this.width = options.width || 1000;
    this.shiftStep = options.shiftStep || 5;
    this.enable = true;
    this.positionX = 0;
}

Scene.prototype.setActivity = function(active = true) {
    this.enable = active;
};

Scene.prototype.move = function() {
    if (!this.enable) {
        return;
    }

    const repeatedShift = this.positionX + this.width;

    this.positionX = repeatedShift <= 0 ? repeatedShift : this.positionX - this.shiftStep;
};

Scene.prototype.render = function() {
    if (!this.enable) {
        return;
    }

    this.object.style.backgroundPositionX = `${this.positionX}px`;
};